import { FaGitlab, FaLinkedin, FaTwitter } from 'react-icons/fa'
import Link from 'next/link'

const socialLinks = [
  {
    icon:<FaGitlab />,
    path: 'https://gitlab.com/raxeraditya'
  },
  {
    icon: <FaTwitter />,
    path: 'https://x.com/K37Aditya'
  },
  {
    icon: <FaLinkedin />,
    path: 'https://www.linkedin.com/in/raxeraditya1/'
  }
]
const Social = () => {
  return (
    <div className='flex gap-6'>
      {socialLinks.map((item, index) => (
        <Link
          target='_blank'
          href={item.path}
          key={index}
          className='duration-3000 flex size-10 items-center justify-center rounded-full border-2 border-blue text-blue hover:bg-blue/20 hover:transition-all'
        >
          {item.icon}
        </Link>
      ))}
    </div>
  )
}

export default Social
